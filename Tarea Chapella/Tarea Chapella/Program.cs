﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tareachapella
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Cantidad de sucursales: ");
			int cant = int.Parse(Console.ReadLine());
			//Constructor
			Sucursal sucur = new Sucursal();
			//Llamando a los metodos
			sucur.Llenardato(cant);
			sucur.PromVentas(cant);
			sucur.sucursalMax(cant);
			sucur.sucursalMin(cant);
			sucur.PromEmpleados(cant);
			sucur.VentaTotal(cant);

		}
	}
}