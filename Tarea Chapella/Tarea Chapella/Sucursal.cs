﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tareachapella
{
	class Sucursal
	{
		public string sucursal { get; set; }
		public int CantClientes { get; set; }
		public int CantEmpleados { get; set; }
		public decimal MontoDeVenta { get; set; }

		public Sucursal() { }

		Sucursal[] sucur = new Sucursal[100];
		public void Llenardato(int cant)
		{
			string[] sucursales = new string[]
			{
				"Once",
				"San Nicolas",
				"Almagro",
				"Microcentro",
				"Palermo",
				"San Cristobal",
				"Parque Patricios"
			};

			for (int i = 0; i < cant; i++)
			{
				sucur[i] = new Sucursal();
				Console.WriteLine("Elija una sucursal: ");
				Console.WriteLine("1. Once");
				Console.WriteLine("2. San Nicolas");
				Console.WriteLine("3. Almagro");
				Console.WriteLine("4. Microcentro");
				Console.WriteLine("5. Palermo");
				Console.WriteLine("6. San Cristobal");
				Console.WriteLine("7. Parque Patricios");
				Console.Write("Opcion: ");
				int opcion = int.Parse(Console.ReadLine());
				switch (opcion)
				{
					case 1:
						sucur[i].sucursal = sucursales[0];
						break;
					case 2:
						sucur[i].sucursal = sucursales[1];
						break;
					case 3:
						sucur[i].sucursal = sucursales[2];
						break;
					case 4:
						sucur[i].sucursal = sucursales[3];
						break;
					case 5:
						sucur[i].sucursal = sucursales[4];
						break;
					case 6:
						sucur[i].sucursal = sucursales[5];
						break;
					case 7:
						sucur[i].sucursal = sucursales[6];
						break;
					default:
						Console.WriteLine("Eliga una opcion valida");
						return;
				}
				Console.Write("Monto de la Venta: ");
				sucur[i].MontoDeVenta = decimal.Parse(Console.ReadLine());
				Console.Write("Cantidad de Empleados: ");
				sucur[i].CantEmpleados = int.Parse(Console.ReadLine());
				Console.Write("Cantidad de Clientes: ");
				sucur[i].CantClientes = int.Parse(Console.ReadLine());
				
				
				
				Console.Write("\n");
			}
		}

		decimal promVentas;
		public void PromVentas(int cant)
		{
			for (int i = 0; i <= cant; i++)
			{
				promVentas += sucur[i].MontoDeVenta;
			}
			decimal finalVentas = promVentas / cant;
			Console.WriteLine("Promedio de Ventas: {0}", finalVentas);
		}
		int promEmpleados;
		public void PromEmpleados(int cant)
		{
			for (int i = 0; i <= cant; i++)
			{
				promEmpleados += sucur[i].CantEmpleados;
			}
			int finalEmpleados = promEmpleados / cant;
			Console.WriteLine("Promedio de Empleados: {0}", finalEmpleados);
		}
		decimal ventaTotal;
		public void VentaTotal(int cant)
		{
			for (int i = 0; i <= cant; i++)
			{
				ventaTotal += sucur[i].MontoDeVenta;
			}
			Console.WriteLine("Venta total: {0}", ventaTotal);
		}
		decimal max;
		string sucur1;
		public void sucursalMax(int cant)
		{
			for (int i = 0; i <= cant; i++)
			{
				if (sucur[i].MontoDeVenta > max)
				{
					max = sucur[i].MontoDeVenta;
					sucur1 = sucur[i].sucursal;
				}
			}
			Console.WriteLine("Venta Maxima: {0}\nSucursal: {1}", max, sucur1);
		}
		decimal min;
		string sucur2;
		public void sucursalMin(int cant)
		{
			min = max;
			for (int i = 0; i <= cant; i++)
			{
				if (sucur[i].MontoDeVenta < min)
				{
					min = sucur[i].MontoDeVenta;
					sucur2 = sucur[i].sucursal;
				}
			}
			Console.WriteLine("Venta Minima: {0}\nSucursal: {1}", min, sucur2);
		}
	}
}